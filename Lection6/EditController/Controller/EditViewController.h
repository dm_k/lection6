//
//  EditViewController.h
//  Lection6
//
//  Created by Vladislav Grigoriev on 07/11/16.
//  Copyright © 2016 Inostudio. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Item.h"

@protocol  EditViewControllerDelegate;

@interface EditViewController : UIViewController

@property (nonatomic, copy) Item *item;
@property (nonatomic, weak) id<EditViewControllerDelegate> delegate;

@end

@protocol  EditViewControllerDelegate <NSObject>

- (void)editViewController:(EditViewController *)controller didCreateItem:(Item *)item;

@end
                
                            
                            
